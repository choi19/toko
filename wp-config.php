<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'toko_online');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'o.<K-ts,#![m6xG)>?K`cEV|h>`e *8A`R+iQkex|8GZ$k&YCszu+;_& [sfhQJy');
define('SECURE_AUTH_KEY',  'X-@ kGkG,/SZ%WewAp.rIIxuk)@YHyp+vAU94H>dyB9ld@Uh-.tZga}P+=*&U<*Q');
define('LOGGED_IN_KEY',    'NPbytb)Y-|mP9EIn1 [P`R?*<g/aE? H5G|S}1>;I#|}D1A3OQOX&2o-HKz7qQ|s');
define('NONCE_KEY',        'y!c[r**UB<Mkt4^oURtM`MFhAy$>9fBT6=q]{h[@8hxupar0}8`YbSrwi.nS$-d0');
define('AUTH_SALT',        'TMwn-BJgnbBrx(l[X%d1bOZkwqG8iq/RfD*=-tCDoq_u-;Jo5@Y3IIwq5Rjs.)nK');
define('SECURE_AUTH_SALT', '01;R^Ewk$<s|O%t V4Y]|-#fLN)|L)L?0v>fu?}efmqyAy(&P70,OZSo+xFg|I,#');
define('LOGGED_IN_SALT',   '8)D8vkv+fDcTihL_er>R^f7|J/s`fSBSkd#UvTH^|.UX$1Oo!Ka!@L7p<k9T?P>S');
define('NONCE_SALT',       'N_@(x+0VL+,q{Sob8q|W,j4}=&Y*}KXrcT<QsMKZ^ANryY,+_{U0!++cNAyegAC3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
