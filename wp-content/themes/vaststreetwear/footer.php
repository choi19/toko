<?php 
/**
* @link http://seintica.com
* @package Wordpress Themes
* @subpackage Vaststreet
* @since Vaststreet 1.0
* @author Seintica_Production
*
*/
?>
<div class="footer">
        <div class="container justfooter">
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-4 customwidthleft">
                    <div class="footerelement">
                        <div class="footerright">
                            <a class="tumblecuk" href="#">
                            </a>
                            <a class="loocuk" href="#">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footerelement">
                    <div class="input-group input-group-sm paksacilik">
                        <input type="text" class="form-control justforfooter">
                        <span class="input-group-addon">Join</span>
                    </div>
                    </div>
                </div>
                <div class="col-md-5 customwidthright">
                    <div class="companion">
                        <a class="seincuk" href="#">
                        </a>
                    </div>
                    <div class="socmed">
                        <div class="social">                        
                            <a class="instacuk" href="#">
                            </a>
                            <a class="vimecuk" href="#">
                            </a>
                            <a class="twitcuk" href="#">
                            </a>
                            <a class="facuk" href="#">
                            </a>
                            <a class="pincuk" href="#">
                            </a>
                            <a>
                            <div class="fb-like" style="float: left; padding-top:3px;" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
                            </a>
                        </div>
                    </div>
                    <div class="kopirik">
                        <small>&copy 2014 vaststreet</small>
                    </div>
                </div>
                <!--
                <div class="pull-left">
                    <div class="tumblr">
                        <a class="tumblecuk" href="#">
                        </a>
                        <a class="loocuk" href="#">
                        </a>
                    </div>
                </div>
                <div class="col-sm-3 subscribe">
                    <div class="input-group form-group-sm">
                        <input type="text" class="form-control input-sm">
                        <span class="input-group-btn">
                        <button class="btn btn-default input-sm" type="button"></button>
                        </span>
                    </div>
                </div>
                <div class="pull-right">
                    <div class="social">                        
                        <a class="instacuk" href="#">
                        </a>
                        <a class="vimecuk" href="#">
                        </a>
                        <a class="twitcuk" href="#">
                        </a>
                        <a class="facuk" href="#">
                        </a>
                        <a class="pincuk" href="#">
                        </a>
                        <a>
                        <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>
                        </a>
                        <a class="seincuk" href="#">
                        </a>
                    </div>
                    <p class="ngambang-right"><small>&copy 2014 vaststreet</small></p>
                </div>
                -->
            </div>
        </div>
    <?php //if (!is_home()) {
        //wp_title('');
    //} 
        wp_footer();
        ?>
    </div>