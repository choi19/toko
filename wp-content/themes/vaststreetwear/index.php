<?php
/**
* @link http://seintica.com
* @package Wordpress Themes
* @subpackage Vaststreet
* @since Vaststreet 1.0
* @author Seintica_Production
*
*/


    get_header(); ?>

    <div class="content">
        <div class="container">
            <div class="col-md-10 col-md-offset-1">
            <?php if ( have_posts() ) : ?>
            <!--Start Loop-->
                <?php while (have_posts()) : the_post(); ?>
                    <?php get_template_part('content', get_post_format()); ?>
                <?php endwhile; ?>

                <?php vaststreet_content_nav('nav-below'); ?>
            <?php else : ?>

            <div class="row">
                <div class="col-xs-12">
                
                <article id="post-0" class="post no-result not-found">
                    <?php 
                        if (current_user_can('edit_post')) :
                            //menampilkan fitur edit untuk user yang sedang login
                    ?>
                <header class="entry-header">
                    <h1 class="entry-title"><?php _e('No post to display', 'vaststreet'); ?></h1>
                </header>
                <div class="entry-content">
                    <p><?php printf(__('Ready to publish your first post? <a href="%s">Get started here</a>.','vaststreet'), admin_url('post-new.php')); ?></p>
                </div>

            <?php else : 
                //menampilkan tampilan default ke semua user
        ?>

        <header class="entry-header">
            <h1 class="entry-title"><?php _e('Nothing Found', 'vaststreet'); ?></h1>
        </header>

        <div class="entry-content">
            <p><?php _e('Apologies, but no result were found. Perhaps searching will help find a related post.','vaststreet'); ?></p>
            <?php get_search_form(); ?>
        </div>
    <?php endif; ?>
                </article>
        <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
    <?php get_footer(); ?>
