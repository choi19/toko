<?php
/**
* @link http://seintica.com
* @package Wordpress Themes
* @subpackage Vaststreet
* @since Vaststreet 1.0
* @author Seintica_Production
*
*/


    get_header(); ?>

	<div class="content">
        <div class="container">
            <div class="col-md-10 col-md-offset-1">

		<?php if ( have_posts() ) : ?>
			<header class="archive-header">
				<h1 class="archive-title"><?php
					if ( is_day() ) :
						printf( __( 'Daily Archives: %s', 'vaststreet' ), '<span>' . get_the_date() . '</span>' );
					elseif ( is_month() ) :
						printf( __( 'Monthly Archives: %s', 'vaststreet' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'vaststreet' ) ) . '</span>' );
					elseif ( is_year() ) :
						printf( __( 'Yearly Archives: %s', 'vaststreet' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'vaststreet' ) ) . '</span>' );
					else :
						_e( 'Archives', 'vaststreet' );
					endif;
				?></h1>
			</header><!-- .archive-header -->

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

				/* Include the post format-specific template for the content. If you want to
				 * this in a child theme then include a file called called content-___.php
				 * (where ___ is the post format) and that will be used instead.
				 */
				get_template_part( 'content', get_post_format() );

			endwhile;

			vaststreet_content_nav( 'nav-below' );
			?>

		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>

		</div>
    </div>
</div>->

<?php get_sidebar(); ?>
<?php get_footer(); ?>