<?php
/**
* @link http://seintica.com
* @package Wordpress Themes
* @subpackage Vaststreet
* @since Vaststreet 1.0
* @author Seintica_Production
*
*/


    get_header(); ?>

<div class="content">
    <div class="container">
    	<div class="col-md-10 col-md-offset-1">
        	<?php while (have_posts()) : the_post(); ?>
        	<?php get_template_part('content', 'page'); ?>
        	<?php //comments_template('', true); ?>
        <?php endwhile; ?>
        </div>
    </div>
</div>

<?php get_sidebar(); ?>
<?php get_footer(); ?>