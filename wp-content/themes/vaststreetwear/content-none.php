<?php
/**
* @link http://seintica.com
* @package Wordpress Themes
* @subpackage Vaststreet
* @since Vaststreet 1.0
* @author Seintica_Production
*
*/
?>

	<article id="post-0" class="post no-results not-found">
		<header class="entry-header">
			<h1 class="entry-title"><?php _e( 'Nothing Found', 'vaststreet' ); ?></h1>
		</header>

		<div class="entry-content">
			<p><?php _e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'vaststreet' ); ?></p>
			<?php get_search_form(); ?>
		</div><!-- .entry-content -->
	</article><!-- #post-0 -->
