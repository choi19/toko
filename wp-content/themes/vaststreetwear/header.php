<?php 
/**
* @link http://seintica.com
* @package Wordpress Themes
* @subpackage Vaststreet
* @since Vaststreet 1.0
* @author Seintica_Production
*
*/
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?php 
            wp_title( '|', true, 'right' );
        ?>
    </title>

    <!-- Bootstrap -->
    <link href="<?php bloginfo( 'template_directory' ); ?>/css/bootstrap.css" rel="stylesheet" media="screen">
    <link rel="stylesheet" type="text/css" href="<?php bloginfo( 'stylesheet_url' ); ?>">
    <link href="<?php bloginfo( 'template_directory' ); ?>/css/aji.css" rel="stylesheet" media="screen">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <link href="<?php bloginfo( 'template_directory' ); ?>/css/jquery.jqzoom.css" rel="stylesheet" media="screen">

    <script src="<?php bloginfo( 'template_directory' ); ?>/js/jquery-1.6.js" type="text/javascript"></script>
    <script src="<?php bloginfo( 'template_directory' ); ?>/js/jquery.jqzoom-core.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.jqzoom').jqzoom({
                    zoomType: 'innerzoom',
                    preloadImages: false,
                    alwaysOn:false
                });
        });
    </script>
    <?php 
        //if (is_singular() && get_option('thread_comments'))
          //  wp_enqueue_script('comment_reply');
        wp_head();
    ?>
    <link rel="icon" type="image/png" href="http://localhost/toko/wp-content/uploads/2014/11/favicon1.png"/>
  </head>
  <body <?php body_class(); ?>>
<div class="header">
        <div class="container">
        <div class="col-md-10 col-md-offset-1">
            <div class="logo"><a href="<?php bloginfo('url'); ?>">
                    <img src="http://localhost/toko/wp-content/uploads/2014/11/vast_asset.png">
                    </a>
                </div>
                <div class="menu">
                    <?php 
                    wp_nav_menu(array('menu_class' => 'nav nav-pills','theme_location' => 'primary'));
                ?>
                </div>
        </div>
    </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php bloginfo( 'template_directory' ); ?>/js/bootstrap.min.js"></script>
  <!--facebook like button-->
  <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=474442852646995&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>